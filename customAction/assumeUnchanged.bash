#!/bin/bash

# Make sure it's chmod 755
# Add to SourceTree in Preferences > Custom Actions 

## For Assume Unchanged :  put '$FILE' in Parameters 
## For Revert Assume Unchanged :  put '$FILE revert' in Parameters

# You can now right click on a file > Custom Actions > Assume Unchanged

if [ -n "$1" ]; then
	if [ -n "$2" ]; then
		if [ $2 == "revert" ];then
			git update-index --no-assume-unchanged $1
			echo "reverted assume-unchanged of $1"	
		else
			echo "Second parameter must be 'revert'"
			exit 1
		fi
	else
		git update-index --assume-unchanged $1
		echo "assume-unchanged of $1"
	fi
fi